

const busboyBodyParser = require('busboy-body-parser');
const bodyParser = require("body-parser");
const mustache = require('mustache-express');
const userJs = require('./models/user.js');
const commentJs = require('./models/comment.js');
const configJs = require('./config.js');
const postJs = require('./models/post.js');
const additionalJs = require('./models/additional.js');
const express = require("express");
const consolidate = require('consolidate');
const path = require('path');

const fs = require("fs").promises;
let mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const serverSalt = configJs.hashingSalt;
const happy = require('./happy.js');
const app = express();


app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  secret: configJs.sessionSecret, // 'SEGReT$25_'
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'mst');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
// new middleware
app.use(bodyParser.urlencoded({ extended: true }));
const auth = require('./auth');
app.use('/auth', auth);




app.post('/deleteComment', function (req, res) {
  commentJs.Comment.deleteById(req.body.id)
    .then(comment => {
      res.redirect('comments');
    })
    .catch(err => {
      res.send(err);
    });
});

app.get("/data/fs/:name", (req, res) => {
  let name = "/data/fs/" + req.params.name;
  res.sendFile(__dirname + name);
});
app.post('/confirmUpdateComment', function (req, res) {

  let id = req.body.commentId;
  let name = req.body.nameComment;
  let content = req.body.contentComment;
  if (req.files.photoFileComment == undefined) {
    let x = { id: id, name: name, content: content }
    commentJs.Comment.update(x)
      .then(comment => { res.redirect(`comments/${comment.id}`) })
      .catch(err => { console.log(err) });
  }
  else {
    let promise1 = new Promise((resolve, reject) => {
      cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
        if (error)
          reject(error)
        else
          resolve(result)
      }).end(req.files.photoFileComment.data);
    })
    promise1.then(result => {

      let pictureUrl = result.secure_url;
      let x = { id: id, name: name, content: content, pictureUrl: pictureUrl };
      commentJs.Comment.update(x)
        .then(comment => { res.redirect(`comments/${comment.id}`) })
        .catch(err => { console.log(err) });
    })
  }
})





app.post('/updateComment', function (req, res) {
  commentJs.Comment.getById(req.body.id).
    then(comment => {
      res.render('updateComment', { comment: comment })
    })
});


function sha512(password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  };
}










const connectOptions = { useNewUrlParser: true };
mongoose.connect(configJs.DataBaseUrl, connectOptions)
  .then(() => console.log('Mongo database connected'))
  .catch(() => console.log('ERROR: Mongo database not connected'));




///застарілий mongoDB
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

/* const configJs = require('./config'); */

const cloudinary = require('cloudinary').v2;
cloudinary.config({
  cloud_name: configJs.cloud_name,
  api_key: configJs.api_key,
  api_secret: configJs.api_secret
});


app.use(busboyBodyParser());

app.use(busboyBodyParser(
  {
    limit: "5mb",
    multi: true,
  }
));
app.use(express.static('public'));
const viewsDir = path.join(__dirname, 'views');

app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');





app.post('/delete', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {
      console.log('\n\n\n\n*')
      console.log(data.public_id);
      let promise1 = new Promise((resolve, reject) => {
        cloudinary.uploader.destroy(data.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
          if (error)
            reject(error)
          else
            resolve(result)
        })
      })
      promise1
        .then(result => {
          console.log(result)
          res.redirect('posts')
        })
        .catch(error => {
          console.log(error);
          res.send("fignya");
        })
        .catch(err => res.send(err));
    })
});







app.post('/addComment', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {
      res.redirect('posts')
    })
    .catch(err => res.send("We have some problem! oops..."));
});



app.post('/uploadNewComment', function (req, res) {

  ////хардкод
  let idOfowner = req.user._id;
  ////хардкод
  let postId = req.body.postId;
  let content = req.body.contentComment;
  let nameOfcomment = req.body.name;
  if (content.length > 200) {
    content = content.substring(0, 200);
  }

  let promise1 = new Promise((resolve, reject) => {
    cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
      if (error)
        reject(error)
      else
        resolve(result)
    }).end(req.files.photoFileComment.data);
  })
  promise1.then(result => {

    let object = new commentJs.Comment(nameOfcomment, idOfowner, postId, content, result.secure_url);

    return commentJs.Comment.insert(object)
  }).then(result => {

    res.redirect(`posts/${postId}`);
  }).catch(err => {
    res.status(500).send("We have some problem! oops...");
    return;
  });
})
app.post('/upload', function (req, res) {
  if (req.body.content.length > 200) {
    req.body.content = req.body.content.substring(0, 200);
  }
  new Promise((resolve, reject) => {
    cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
      if (error)
        reject(error)
      else
        resolve(result)
    }).end(req.files.photoFile.data)
  }).then(result => {


    return additionalJs.getPostFromRequestFromCreatingNewPost(req.body.topic, req.body.name, result.secure_url, req.body.content, result.public_id);

  }).then(result => {
    res.redirect(`/posts/${result._id}`);
    console.log(result);
  }).catch(err => {
    console.log(err);
    res.status(500).send("We have some problem! oops...");
    return;
  });
});


function makeGetter(name) {
  app.get('/' + name, function (req, res) {
    if (req.user == undefined) {
      res.render(name, { guest: 'guest' });
    }
    else if (req.user.role == 'admin') {
      res.render(name, { admin: req.user.login });
    }
    else {
      res.render(name, { user: req.user.login });
    }


  });
}
function forEach__(list, ln) {
  for (let el of list) {
    ln(el);
  }
}
forEach__(["about"], makeGetter);




//promise
app.get('/users', function (req, res) {
  if (req.user.role == "admin") {
    userJs.User.getAll()
      .then(
        (items) => {
          res.status(200).render('users', { items: items });
        })
      .catch(
        (err) => {
          res.status(500).send("We have some problem! oops...");
        })
  }
  else {
    res.redirect('/');
  }

})
//promise
app.get('/posts', function (req, res) {
  if (req.user) {
    let user = req.user;
    console.log(req.user);
    postJs.Post.getAll()
      .then(data => {
        let dataObj = {};
        dataObj['items'] = data;
        dataObj = additionalJs.PaginationAndSearch(req, dataObj);
        if (user.role == 'user') {
          dataObj.user = user.login;
          res.status(200).render('posts', dataObj)
        }
        else {
          dataObj.admin = user.login;
          res.status(200).render('posts', dataObj)
        }

      })
      .catch(err => res.status(500).send(err.toString()))
  }
  else {
    res.redirect('/auth/register');
  }

})
app.post('/changeRole', function (req, res) {
  console.log("hello");
  let role;
  if (req.body.role == 'user') {
    role = 'admin';
  }
  if (req.body.role == 'admin') {
    role = 'user';
  }
  let x = { role: role, id: req.body.id }
  console.log(x.id)
  console.log(x);
  userJs.User.updateRole(x)
    .then(user => res.redirect(`users/${x.id}`))
    .catch(error => {
      console.log(error);
      res.send(error);
    })



})

app.get('/comments', function (req, res) {

  if (req.user) {
    if (req.user.role == 'admin') {
      console.log(req.user);
      commentJs.Comment.getAll()
        .then(comments => {




          let commentsObj = {};
          commentsObj['items'] = comments;
          commentsObj = additionalJs.PaginationAndSearch(req, commentsObj);
          commentsObj.admin = req.user.login;
          res.render("comments", commentsObj);
        })
        .catch(err => {
          console.log(err);
          res.status(404).send(err);
        })

    }
    else {
      console.log(req.user);
      commentJs.Comment.getAll()
        .then(comments => {
          let commentsObj = {};
          commentsObj['items'] = comments;
          commentsObj.user = req.user.login;
          res.render("comments", commentsObj);
        })
        .catch(err => {
          console.log(err);
          res.status(404).send(err);
        })

    }

  }
  else {
    res.redirect('auth/register');
  }

});


app.get('/users/:id', function (req, res) {

  userJs.User.getById(req.params.id)
    .then(user => res.status(200).render('user', user))
    .catch(err => res.status(404).redirect('http://www.romainbrasier.fr/404.php?lang=en'));

});

app.get('/comments/:id', function (req, res) {
  if (req.user) {
    commentJs.Comment.getById(req.params.id)

      .then(comment => {
        let commentObj = {};
        commentObj.comment = comment;
        if (req.user.role = 'admin') {
          commentObj.admin = req.user.login;
        }
        else {
          commentObj.user = req.user.login;
        }
        res.render('comment', commentObj)
      })
      .catch(err => res.send(err));
  }
  else {
    res.redirect('auth/register');
  }
});

app.get('/index', function (req, res) {
  if (req.user) {
    if (req.user.role == 'admin') {
      res.render('index', { admin: req.user.login });
    }
    else {
      res.render('index', { user: req.user.login });
    }
  }
  else
    res.render('index', { guest: 'guest' });
});

app.get('/posts/new', function (req, res) {
  if (req.user) {
    res.status(200).render('new');
  }
  else {
    res.redirect('/auth/register');
  }
}


);
//promise
app.get('/users/:id', function (req, res) {
  if (req.user.role == 'admin') {
    userJs.User.getById(req.params.id)
      .then(user => res.status(200).render('user', user))
      .catch(err => res.status(404).redirect('http://www.romainbrasier.fr/404.php?lang=en'));
  }




});
app.post('/delete', (req, res) => {
  if (req.user) {
    console.log(req.body.id)
    postJs.Post.deleteById(req.body.id)
      .then(data => {
        res.redirect('posts')
      })
      .catch(err => res.send("We have some problem! oops..."));
  }
  else {
    res.redirect('auth/register');
  }


});
app.get('/posts/:id', function (req, res) {
  if (req.user) {
    postJs.Post.getById(req.params.id)
      .then(data => {
        return Promise.all([data, userJs.User.getById(data.idOfowner)])
      })
      .then(([data, user]) => {
        if (user != null) {
          data['avaUrl'] = user.avaUrl;
        }
        return Promise.all([data, commentJs.Comment.getAll(data.id)])
      })
      .then(([data, arrayofComments]) => {
        data.nameOfmyPenis = arrayofComments;
        let dataObj = {};
        dataObj['data'] = data;
        if (req.user.role == 'admin') {
          dataObj['admin'] = req.user.login;
        }
        else if (req.user.role == 'user') {
          dataObj['user'] = req.user.login;
        }

        res.status(200).render('post', dataObj);
      })
      .catch(err => {

        res.status(404).send("This post is not found! Error 404")
      });
  }
  else {
    res.redirect('auth/register');
  }
});

app.get('/api/users/:id', function (req, res) {
  userJs.User.getById(req.params.id)
    .then(user => {
      user = JSON.stringify(user);
      res.set({ "Content-type": "application/json" }).status(200).send(user)
    })
    .catch(err => res.status(404).redirect('http://www.romainbrasier.fr/404.php?lang=en'))
});



//lab7
app.get('/',
  (req, res) => {

    let user = req.user;
    if (user === undefined) {
      res.render('index', { guest: 'guest' })
    }
    else if (user.role == 'admin') {
      res.render('index', { admin: user.login });
    }
    else {

      res.render('index', { user: user.login });

    }



  }
);





///ти так експортиш всю парашу, яку ти написав в app.js до www
module.exports = app;

