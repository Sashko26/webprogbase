const config = require('./config.js');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});

cloudinary.v2.uploader.upload_stream("./data/fs/a.jpg",
    function (error, result) {
        console.log(error);
        console.log(result);
    });






app.post('/upload', function (req, res) {
    let photoName = req.files.photoFile.name;


    let promise1 = new Promise((resolve, reject) => {
        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
            if (error)
                reject(error)
            else
                resolve(result)
        }).end(req.files.photoFile.data)
    })
    promise1.then(result => { res.send(result) })
    return;
});





