
let fs = require('fs').promises;
let mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    content: { type: String },
    idOfowner: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    topic: { type: String },
    name: { type: String },
    dateOfpublication: { type: Date, default: Date.now },
    pictureUrl: { type: String },
    comments : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
    public_id: {type: String}


});
const PostModel = mongoose.model("Post", PostSchema);
class Post {

    constructor(idOfowner = null, topic = null, name = null, pictureUrl = null, content = null,public_id=null) {
        this.content = content;
        this.idOfowner = idOfowner;
        this.topic = topic;
        this.name = name;
    /*     this.dateOfpublication = dateOfpublication; */
        this.pictureUrl = pictureUrl;
        this.public_id=public_id;
        this.dateOfpublication= new Date();
        
    }

    static insert(x) {
      
        return new PostModel(x).save();
    }


    //поломався пошук.
    static getAll() {
        return PostModel.find();
    }
    static getById(id) 
    {
        return PostModel.findById(id);     
    }
    static update(x) {
       
      return PostModel.findByIdAndUpdate(x.id, {comments : x.comments});
    }
    static deleteById(id) {
        return PostModel.findByIdAndDelete(id);
    }
}
module.exports = { Post ,PostModel,PostSchema};