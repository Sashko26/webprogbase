let fs = require('fs').promises
let mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    role: { type: String, default: "user" },
    login: { type: String, required: true },
    hashedPassword: { type: String, required: false },
    fullname: { type: String, default: " " },
    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String },
    isDisabled: { type: Boolean },
    posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
    biography: { type: String },
    public_id: { type: String }
});
const UserModel = mongoose.model('User', UserSchema);
class User {

    static insert(x) {
        return new UserModel(x).save();
    }
    static getAll() {
        return UserModel.find();
    }
    static getById(id) {

        return UserModel.findById(id);
    }
    static getByLoginAndHashedPass(username, hashedPassword) {
        return UserModel.findOne({ login: username, hashedPassword: hashedPassword });
    }
    static updateRole(x) {
        console.log(x);
        return UserModel.updateOne({_id : x.id}, {role:x.role});
    }
}
module.exports = { User };



