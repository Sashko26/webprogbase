const busboyBodyParser = require('busboy-body-parser');
const mustache = require('mustache-express');
const userJs = require('./models/user.js');
const postJs = require('./models/post.js');
const express = require("express");
const consolidate = require('consolidate');
const path = require('path');
const app = express();
const fs = require("fs");

app.use(busboyBodyParser());

app.use(busboyBodyParser(
  {
    limit: "5mb",
    multi: true,
  }
));R
app.use(express.static('public'));
app.use(express.static(path.join(__dirname,'data/fs')));
const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
let searchStr;


app.get("/data/fs/:name", (req, res) => {
  let name = "/data/fs/" + req.params.name;
  console.log(name);
  res.sendFile(__dirname + name);
});


app.post('/delete', (req, res) => {
  console.log(req.body.id);
  let id = parseInt(req.body.id);
  postJs.Post.deleteById(id, (err, data) => {
    if (err) console.log(err);
    else res.redirect('posts');
  })

})
app.post('/upload', function (req, res) {
  let photoName = req.files.photoFile.name;
  let post = getPostFromRequestFromCreatingNewPost(req.body.topic, req.body.name, 'http://localhost:3000/data/fs/' + photoName, req.body.content);
  postJs.Post.insert(post, (err, data) => {
    if (err) console.log(err);
    else {
      fs.writeFile(__dirname + "/data/fs/" + photoName, req.files.photoFile.data, (err) => {
        if (err) console.log(err.toString());
        res.redirect('/posts/' + data);
      });
    }
  });
})
function makeGetter(name) {
  app.get('/' + name, function (req, res) {
    console.log(name);
    res.render(name);

  });
}
function forEach__(list, ln) {
  for (let el of list) {
    ln(el);
  }
}
forEach__(["index", "about"], makeGetter);


app.get('/users', function (req, res) {
  userJs.User.getAll((err, data) => {
    if (data) {
      let items = data.items;
      res.status(200).render('users', { items: items });
    }
    if (err)
      res.status(500).send("We have some problem! oops...");
  });

});
app.get('/posts', function (req, res) {
  postJs.Post.getAll((err, data) => {
    if (data) {
      data = PaginationAndSearch(req, data);
      res.status(200).render('posts', data);
    }
    if (err)
      res.status(500).send("We have some problem! oops...");
  });
});

app.get('/', function (req, res) {
  res.render('index');
});

app.get('/posts/new', function (req, res) {
  res.status(200).render('new');
}

);
app.get('/users/:id', function (req, res) {
  userJs.User.getById(Number.parseInt(req.params.id), (err, data) => {
    if (data) {
      res.status(200).render('user', data);
    }
    else
      res.status(404).send(err);
  });
});

app.get('/posts/:id', function (req, res) {
  postJs.Post.getById(Number.parseInt(req.params.id), (err, data) => {
    if (data) {
      userJs.User.getById(Number.parseInt(data.idOfowner), (error, user) => {
        if (user) {
          data["avaUrl"] = user.avaUrl;
          res.status(200).render('post', data);
        }
        else console.log(error);
      });
    }
    else {
      console.log(err);
      res.status(404).send("This post is not found! Error 404");
    }
  });
});

app.get('/api/users/:id', function (req, res) {
  userJs.User.getById(Number.parseInt(req.params.id), (err, data) => {
    if (err) {
      console.log(err);
      res.status(404).send("This user is not found! Error 404");
    }
    if (data) {
      res.set({ "Content-type": "application/json" }).status(200).send(data);
    }
  });
});



//additional functions
function PaginationAndSearch(req, data) {
  if (searchStr === undefined && req.query.searchStr === undefined) {
    data['searchStr'] = "";
  }
  else if (req.query.searchStr === undefined && searchStr !== undefined) {
    data['searchStr'] = searchStr;
  }
  else if (req.query.searchStr !== undefined) {
    searchStr = req.query.searchStr;
    data['searchStr'] = req.query.searchStr;
  }
  if (searchStr) {
    data.searchStr = searchStr;
    let items = [];
    for (let post of data.items) {
      if (post.name.includes(searchStr)) {
        items.push(post);
      }
    }
    data.items = items;
  }
  const sizeOfpage = 3;
  let page = req.query.page || 1;
  let amountOfPages = data.items.length / sizeOfpage;
  if (amountOfPages == 0) {
    if (req.query.searchStr != undefined) {
      data['weHaveNotСoincidence'] = true;
    }
    else {
      data['weHaveNotСoincidence'] = false;
    }
    amountOfPages = 1;
  }
  if (amountOfPages - parseInt(amountOfPages) !== 0) {
    amountOfPages = parseInt(amountOfPages) + 1;
  }
  if (page > amountOfPages) {
    page = amountOfPages;
  }
  if (page < 1) {
    page = 1;
  }
  let prevIsUnable = false;
  let nextIsUnable = false;
  let start = sizeOfpage * (page - 1);
  let end = sizeOfpage * page;
  data.items = data.items.slice(start, end);
  if (page == 1) {
    prevIsUnable = true;
  }
  if (page == amountOfPages) {
    nextIsUnable = true;
  }
  req.query.page = page;
  let nextPage = parseInt(page) + 1;
  let prevPage = page - 1;

  data["nextPage"] = nextPage;
  data["prevPage"] = prevPage;
  data["page"] = parseInt(page);
  data["prevIsUnable"] = prevIsUnable;
  data["nextIsUnable"] = nextIsUnable;
  data["amountOfPages"] = amountOfPages;
  return data;
}

function getPostFromRequestFromCreatingNewPost(topic, name, pictureUrl, content) {
  let date = new Date();
  let post = new postJs.Post(0, 0, topic, name, date, pictureUrl, content);
  return post;
}



///ти так експортиш всю парашу, яку ти написав в app.js до www
module.exports = app;
















