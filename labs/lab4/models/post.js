
let fs = require('fs');
class Post {
    constructor(id = 0, idOfowner = 0, topic = null, name = null, dateOfpublication = null, pictureUrl = null, content = null) {
        this.content = content;
        this.id = id;
        this.idOfowner = idOfowner;
        this.topic = topic;
        this.name = name;
        this.dateOfpublication = dateOfpublication;
        this.pictureUrl = pictureUrl;
    }
    static insert(x, callback) {
        fs.readFile("./data/posts.json", 'utf8', (err, data) => {
            if (err) callback(err, null);
            else {
                let text = JSON.parse(data);
                x.id = text.nextId;
                text.nextId++;
                text.items.push(x);
                data = JSON.stringify(text, null, 4);
                fs.writeFile("./data/posts.json", data, (errw) => {
                    callback(errw, x.id);
                });
            }
        });
    }
    static getAll(callback) {
        fs.readFile("./data/posts.json", 'utf8', (err, data) => {
            callback(err, JSON.parse(data));
        });
    }
    static getById(id, callback) {
        fs.readFile("./data/posts.json", 'utf8', (err, textJson) => {
            if (textJson) {
                let error;
                let post = JSON.parse(textJson).items.find(function (i) { return i.id === id });
                if (post === undefined) {
                    error = new Error("Post with ID: " + id + " does not exist!");
                    callback(error, null);
                    return;
                }
                callback(null, post);
            }

        });
    }
    static update(x) {
        let textJson = fs.readFileSync("./data/posts.json", 'utf8');
        let text = JSON.parse(textJson);
        for (let i of text.items) {
            if (i.id === x.id) {
                let idOwnerChanged = true;
                let topicChanged = true;
                let nameChanged = true;
                if (x.idOfowner === null) {
                    x.idOfowner === i.idOfowner;
                    idOwnerChanged = false;
                }
                if (x.topic === null) {
                    x.topic === i.topic;
                    topicChanged = false;
                }
                if (x.name === null) {
                    x.name === i.name;
                    nameChanged = false;
                }
                if (x.dateOfpublication === null) {
                    x.dateOfpublication === i.dateOfpublication;
                }
                if (idOwnerChanged === true) {
                    i.idOfowner = x.idOfowner;
                }
                if (topicChanged === true) {
                    i.topic = x.topic;
                }
                if (nameChanged === true) {
                    i.name = x.name;
                }
                textJson = JSON.stringify(text, null, 4);
                fs.writeFileSync("./data/posts.json", textJson);
                return true;
            }
        }
        return false;
    }
    static deleteById(id, callback) {
        fs.readFile("./data/posts.json", 'utf8', (err, data) => {
            data = JSON.parse(data);
            let arr = data.items;
            let deletedPost;

            for (let i = 0; i < arr.length; i++) {
                if (arr[i].id === id) {
                    deletedPost = arr[i].id;
                    for (let j = i; j < arr.length - 1; j++) {
                        data.items[j] = data.items[j + 1];
                    }
                    data.items.pop();
                    break;
                }
            }
            data = JSON.stringify(data, null, 4);
            fs.writeFile("./data/posts.json", data, () => {
                if (deletedPost == undefined) {
                    let err = new Error("This post with ID " + id + " do not exists!");
                    callback(err, null)
                    return;
                }
                callback(null, deletedPost);
            });

        });
    }
}

module.exports = { Post };