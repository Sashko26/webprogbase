let fs = require('fs');
class User {
    constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
        this.id = id;
        this.role = role;
        this.login = login;
        this.fullname = fullname;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
    }
    static getAll(callback) {
        fs.readFile("./data/users.json", 'utf8', (err, data) => {
            callback(err, JSON.parse(data));
        });
    }
    static getById(id, callback) {
        fs.readFile("./data/users.json", 'utf8', (err, textJson) => {
            if (textJson) {
                let error;
                let user = JSON.parse(textJson).items.find(function (i) { return i.id === id });
                if (user === undefined) {
                    error = new Error("User with ID: " + id + "does not exist!");
                }
                callback(error, user);
            }
        });
    }
}
module.exports = { User };



