
const postJs = require('./post.js');
let homer = "5dc6c560f13c0a3982f02ce7";
let bender = "5dc6c560f13c0a3982f02ce7";


let searchStr;
//additional functions
function PaginationAndSearch(req, data) {
    if (searchStr === undefined && req.query.searchStr === undefined) {
      data['searchStr'] = "";
    }
    else if (req.query.searchStr === undefined && searchStr !== undefined) {
      data['searchStr'] = searchStr;
    }
    else if (req.query.searchStr !== undefined) {
      searchStr = req.query.searchStr;
      data['searchStr'] = req.query.searchStr;
    }
    if (searchStr) {
      data.searchStr = searchStr;
      let items = [];
      for (let post of data.items) {
        if (post.name.includes(searchStr)) {
          items.push(post);
        }
      }
      data.items = items;
    }
    const sizeOfpage = 3;
    let page = req.query.page || 1;
    //а тут можна подумати над тимЁ що повернутих результатыв може бути 2 а сторынка на 3 елементи
    let amountOfPages = data.items.length / sizeOfpage;
    if (amountOfPages == 0) {
      if (req.query.searchStr != undefined) {
        data['weHaveNotСoincidence'] = true;
      }
      else {
        data['weHaveNotСoincidence'] = false;
      }
      amountOfPages = 1;
    }
    if (amountOfPages - parseInt(amountOfPages) !== 0) {
      amountOfPages = parseInt(amountOfPages) + 1;
    }
    if (page > amountOfPages) {
      page = amountOfPages;
    }
    if (page < 1) {
      page = 1;
    }
    let prevIsUnable = false;
    let nextIsUnable = false;
    let start = sizeOfpage * (page - 1);
    let end = sizeOfpage * page;
    data.items = data.items.slice(start, end);
    if (page == 1) {
      prevIsUnable = true;
    }
    if (page == amountOfPages) {
      nextIsUnable = true;
    }
    req.query.page = page;
    let nextPage = parseInt(page) + 1;
    let prevPage = page - 1;
  
    data["nextPage"] = nextPage;
    data["prevPage"] = prevPage;
    data["page"] = parseInt(page);
    data["prevIsUnable"] = prevIsUnable;
    data["nextIsUnable"] = nextIsUnable;
    data["amountOfPages"] = amountOfPages;
    return data;
  }
  function getPostFromRequestFromCreatingNewPost(idOfowner, name, pictureUrl, content,public_id) {
    let date = new Date();

    
  //тут відбувається хардкоджена прив'язка юзера до поста.
   let post = {idOfowner:idOfowner,name:name,pictureUrl:pictureUrl,content:content,public_id:public_id};
     /*  let post = new postJs.Post(idOfowner,name,pictureUrl,content,public_id); */

      console.log(post);
    return  postJs.Post.insert(post);
    
   
  }
  module.exports = {getPostFromRequestFromCreatingNewPost,PaginationAndSearch}; 