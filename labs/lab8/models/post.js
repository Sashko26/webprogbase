
let fs = require('fs').promises;
let mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    content: { type: String },
    idOfOwner: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: { type: String },
    dateOfpublication: { type: Date, default: Date.now },
    pictureUrl: { type: String },
    public_id: {type: String}


});
const PostModel = mongoose.model("Post", PostSchema);
class Post {

    constructor(idOfOwner = null, topic = null, name = null, pictureUrl = null, content = null,public_id=null) {
        this.content = content;
        this.idOfOwner = idOfOwner;
        this.topic = topic;
        this.name = name;
    /*     this.dateOfpublication = dateOfpublication; */
        this.pictureUrl = pictureUrl;
        this.public_id=public_id;
        this.dateOfpublication= new Date();
        
    }

    static insert(x) {
      
        return new PostModel(x).save();
    }


    //поломався пошук.

    static deleteAllPostsThatBelongUser(idOfOwner)
    {
    return PostModel.remove({idOfOwner:idOfOwner});
    }
    static getAll() {
        return PostModel.find();
    }
    static getAllThatBelongToUser(idOfOwner)
    {
        return PostModel.find({ idOfOwner: idOfOwner});
    }
    static getById(id) 
    {
        return PostModel.findById(id);     
    }
    static update(x) {
       
        if (x.pictureUrl == undefined) {
            console.log("hello kuki undefined");
            console.log(x.name + "our undatedName post!!!!!!!!");
            console.log(x.content + "our undatedContent post!!!!!!!!");
            console.log(x.id + "our id post!!!!!!!!");
            return PostModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content });
        }
        else {
            console.log("hello kuki ");
            console.log(x + "our undated post!!!!!!!!");
            return PostModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content, pictureUrl: x.pictureUrl,public_id: x.public_id},{new: true});
        }
    }
    static deleteById(id) {
        return PostModel.findByIdAndDelete(id);
    }
}
module.exports = { Post ,PostModel,PostSchema};