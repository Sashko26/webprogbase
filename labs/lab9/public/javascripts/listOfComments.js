console.log("Loading Browser JS app.js");
Promise.all([
    fetch("/templetes/listOfComments.mst").then(x => x.text()),
    fetch("/api/v1/comments").then(x => x.json()),
])
    .then(([templateStr, itemsData]) => {
        console.log('templateStr', templateStr);
        console.log('itemsData', itemsData);
        const dataObject = {items: itemsData};
        const renderedHtmlStr = Mustache.render(templateStr, dataObject);
        return renderedHtmlStr;
    })
    .then(htmlStr => {
        console.log('htmlStr', htmlStr);
        const appEl = document.getElementById('listOfComments');
        appEl.innerHTML = htmlStr;
    })
    .catch(err => console.error(err));