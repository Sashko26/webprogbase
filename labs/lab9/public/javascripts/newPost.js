window.onload = function () {
    //username
    let elementButtonConfirm = document.getElementById("newPostButton");
    let elementNameOfPost = document.getElementById("nameOfPost");
    elementNameOfPost.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementNameOfPost.validity.valid) {
            elementNameOfPost.classList.remove("error-style");
            document.getElementById('nameDiv').innerHTML = '&nbsp';
        }
        else {
            elementNameOfPost.classList.add("error-style");
            document.getElementById('nameDiv').innerHTML = 'This field is required!';

        }
    }, false);


    let elementContentOfPost = document.getElementById("contentOfPost");
    elementContentOfPost.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementContentOfPost.validity.valid) {
            elementContentOfPost.classList.remove("error-style");
            document.getElementById('contentDiv').innerHTML = '&nbsp';
        }
        else {
            elementContentOfPost.classList.add("error-style");
            document.getElementById('contentDiv').innerHTML = 'This field is required!';
        }
    }, false);



    let elementNewPostForm = document.getElementById("newPost");
    elementNewPostForm.addEventListener('input', function (event) {
        elementNewPostForm.disabled = true;
        if (elementContentOfPost.validity.valid && elementNameOfPost.validity.valid) {
            elementButtonConfirm.disabled = false;
        }
    }, false);
}