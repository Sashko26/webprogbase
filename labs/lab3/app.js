
const userJs = require('./models/user.js');
const postJs = require('./models/post.js');
const express = require("express");
const consolidate = require('consolidate');
const path = require('path');
const mustache = require('mustache-express');


const fs = require("fs");

let app = express();
// will open public/ directory files for http requests
app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

function makeGetter(name) {
  app.get('/' + name, function (req, res) {
   console.log(name);
   res.render(name);
    
  });
}
function forEach__(list, ln) {
  for (let el of list) {
    ln(el);
  }
}
forEach__(["index","about"], makeGetter);








app.get('/users',function(req,res)
{
  let items = userJs.User.getAll(); 
  res.render('users', {items : items});
})
app.get('/posts',function(req,res)
{
  let items = postJs.Post.getAll(); 
  res.render('posts', {items});
  
})
app.get('/' ,function (req, res) {
  res.render('index');
});
app.get('/users/:id',function (req, res) {
  let value = userJs.User.getById(Number.parseInt(req.params.id));
  if(value===undefined)
  res.status(404).send("This user is not fount! Error 404");   
  else
 res.status(200).render('user',value);

 console.log(value);
});
app.get('/posts/:id',function (req, res) {
  let value = postJs.Post.getById(Number.parseInt(req.params.id));
  if(value===undefined)
  res.status(404).send("This post is not fount! Error 404");   
  else
  /* res.mustache.render */
  {
      let avaUrl= userJs.User.getById(value.idOfowner).avaUrl;
      console.log(avaUrl);
      value["avaUrl"]=avaUrl;
      /* let object = {value:value, avaUrl:avaUrl}   */    
      res.status(200).render('post',value);
      console.log(value.idOfowner);
  }
 console.log(value);
});
app.get('/api/users/:id', function (req, res) {
  let user = JSON.stringify(userJs.User.getById(Number.parseInt(req.params.id)));
  console.log(user);
  if(user===undefined)
  res.status(404).send("This user is not fount! Error 404");   
  else
  {
    res.status(200).send(user).set({"Content-type":"application/json"});
  }
 
});

/* app.get("/images/users/:name",function(req,res){
    let name = req.params.name;
    res.download(_dirname+ "/images/"+name);
}) */
module.exports = app;
