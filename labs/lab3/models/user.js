class User {
    constructor(id, login, role, fullname, registeredAt, avaUrl, isDisabled) {
        this.id = id;
        this.role = role;
        this.login = login;
        this.fullname = fullname;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
    }

    static getAll() {
        let fs = require('fs');
        let textJson = fs.readFileSync("./data/users.json", 'utf8');
        let arr = JSON.parse(textJson);
        return arr.items;
    }
    static getById(id) {

        let fs = require('fs');
        let textJson = fs.readFileSync("./data/users.json", 'utf8');
        let text = JSON.parse(textJson);

        for (let i of text.items) {
            if (i.id === id) {
                return i;
            }
        }

    }

}
module.exports = { User };



