const busboyBodyParser = require('busboy-body-parser');
const mustache = require('mustache-express');
const userJs = require('./models/user.js');
const commentJs = require('./models/comment.js');
const postJs = require('./models/post.js');
const additionalJs = require('./models/additional.js');
const express = require("express");
const consolidate = require('consolidate');
const path = require('path');
const app = express();
const fs = require("fs").promises;
let mongoose = require('mongoose');
let homer = "5db5e759d00d1de2f76ada0a";


const dbUrl = 'mongodb://localhost:27017/DataBase';
const connectOptions = { useNewUrlParser: true };
mongoose.connect(dbUrl, connectOptions)
  .then(() => console.log('Mongo database connected'))
  .catch(() => console.log('ERROR: Mongo database not connected'));


const PostModel = mongoose.model('Post', postJs.Post.PostSchema);
const UserModel = mongoose.model('User', userJs.User.UserSchema);
const CommentModel = mongoose.model('Comment', commentJs.Comment.CommentSchema);

///застарілий mongoDB
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

//тимчасові змінні в 7 лабі пропадуть

let bender = "5db5e76ad00d1de2f76ada18";
app.use(busboyBodyParser());

app.use(busboyBodyParser(
  {
    limit: "5mb",
    multi: true,
  }
));
app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');

app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
let searchStr;


app.get("/data/fs/:name", (req, res) => {
  let name = "/data/fs/" + req.params.name;
  res.sendFile(__dirname + name);
});


app.post('/delete', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {
      res.redirect('posts')
    })
    .catch(err => console.log(err));
});



app.post('/confirmUpdateComment', function (req, res) {

  let id = req.body.commentId;
  console.log(id);
  let name = req.body.nameComment;
  let content = req.body.contentComment;
  if (req.files.photoFileComment == undefined) {
    let x = { id: id, name: name, content: content }
    commentJs.Comment.update(x)
      .then(comment => { res.redirect(`comments/${comment.id}`) })
      .catch(err => { console.log(err) });
  }
  else {

    let photoPath = 'http://localhost:3000/data/fs/' + req.files.photoFileComment.name
    let x = { id: id, name: name, content: content, pictureUrl: photoPath };
    console.log(__dirname + req.files.photoFileComment.name + " paraska");
    console.log(photoPath + "qweqweqwe");
    let path = __dirname + "/data/fs/" + req.files.photoFileComment.name;
    console.log(path);


    fs.writeFile(path, req.files.photoFileComment.data).
      then(err => {
        commentJs.Comment.update(x)
          .then(data => {
            res.redirect(`comments/${id}`)
          })
          .catch(err => {
            console.log(err);
            res.status(500).send("We have some problem! oops...")
          });

      }).catch(err => console.error(err))
  }
})

app.post('/updateComment', function (req, res) {
  commentJs.Comment.getById(req.body.id).
    then(comment => {
      console.log(comment);
      res.render('updateComment', { comment: comment })
    })
});


app.post('/deleteComment', function (req, res) {
  commentJs.Comment.deleteById(req.body.id)
    .then(comment => {
      return postJs.Post.getById(req.body.idOfPost)
    })
    .then(post => {

      let index = post.comments.indexOf(req.body.id);
      return post.comments.splice(index, 1);

    })
    .then(whatIsIt => {
      console.log(whatIsIt);
      return res.redirect('comments');
    })
    .catch(err => {
      res.send(err);
    });
});


app.post('/addComment', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {
      res.redirect('posts')
    })
    .catch(err => console.log(err));
});


app.post('/uploadNewComment', function (req, res) {
  ////хардкод
  let idOfowner = "5dbb1aebd1296c0017858b16";


  ////хардкод




  let photoName = req.files.photoFileComment.name;
  let postId = req.body.postId;
  let content = req.body.contentComment;
  let nameOfcomment = req.body.name;
  if (content.length > 200) {
    content = content.substring(0, 200);
  }
  let comment = new commentJs.Comment(nameOfcomment, idOfowner, postId, content, 'http://localhost:3000/data/fs/' + photoName);

  postJs.Post.getById(postId)
    .then(postObj => {

      return Promise.all([postObj, commentJs.Comment.insert(comment)])
    })
    .then(([postObj, commentObj]) => {
      postObj.comments.push(commentObj.id);
      return postObj;
    })
    .then(postObj => {
      return postJs.Post.update(postObj)
    })
    .then(postUpd => {
      fs.writeFile(__dirname + "/data/fs/" + photoName, req.files.photoFileComment.data, (err) => {
        if (err) console.log(err.toString());
      });
      return res.redirect(`/posts/${postId}`);
    })
    .catch(error => console.log(error));

})




app.get('/comments', function (req, res) {

  commentJs.Comment.getAll()
    .then(comments => {
      let commentsObj = {};

      commentsObj['items'] = comments;
      res.render("comments", commentsObj);

    })
    .catch(err => {
      console.log(err);
      res.status(500).send(err);
    })

});




app.get('/comments/:id', function (req, res) {

  commentJs.Comment.getById(req.params.id)
    .then(comment => {
      if(req.user)
      {
        console.log("GIve me idOfPost- " + comment.idOfPost);
        let objComment = {};
        objComment.comment= comment;
        objComment.
        res.render('comment', { comment: comment })
      }      
      else
      {

      }
     
    })
    .catch(err => {
      res.status(404).redirect('https://9gag.com/404');
      console.log(err);
    });
});

//////не забуть , що у тебе тут хардкоджений 3000 порт
app.post('/upload', function (req, res) {
  let photoName = req.files.photoFile.name;
  ///typeof для 7 лаби.
  //вставляє новий пост в базу даних
  if (req.body.content.length > 200) {
    req.body.content = req.body.content.substring(0, 200);
  }

  let post = additionalJs.getPostFromRequestFromCreatingNewPost(req.body.topic, req.body.name, 'http://localhost:3000/data/fs/' + photoName, req.body.content);
  fs.writeFile(__dirname + "/data/fs/" + photoName, req.files.data, (err) => {
    if (err) console.log(err.toString());
    res.redirect('/posts/' + data);
  });
  postJs.Post.insert(post)
    .then(data => {
      fs.writeFile(__dirname + "/data/fs/" + photoName, req.files.photoFile.data, (err) => {
        if (err) console.log(err.toString());
      });
      res.redirect('/posts/' + data.id);
    })
    .catch(err => {
      res.status(500).send("We have some problem! oops...");
    });
})
function makeGetter(name) {
  app.get('/' + name, function (req, res) {
    res.render(name);

  });
}
function forEach__(list, ln) {
  for (let el of list) {
    ln(el);
  }
}
forEach__(["index", "about"], makeGetter);

//promise
app.get('/users', function (req, res) {
  userJs.User.getAll()
    .then(
      (items) => {
        res.status(200).render('users', { items: items });
      })
    .catch(
      (err) => {
        res.status(500).send("We have some problem! oops...");
      })
})
//promise
app.get('/posts', function (req, res) {
  postJs.Post.getAll()
    .then(data => {
      let dataObj = {};
      dataObj['items'] = data;
      dataObj = additionalJs.PaginationAndSearch(req, dataObj);
      res.status(200).render('posts', dataObj)
    })
    .catch(err => res.status(500).send(err.toString()))
})


app.get('/', function (req, res) {
  res.render('index');
});

app.get('/posts/new', function (req, res) {
  res.status(200).render('new');
});


//promise
app.get('/users/:id', function (req, res) {
  userJs.User.getById(req.params.id)
    .then(user => res.status(200).render('user', user))

    .catch(err => {
      res.status(404).redirect('https://9gag.com/404');
      console.log(err);
    });
});


app.get('/posts/:id', function (req, res) {
  postJs.Post.getById(req.params.id)
    .then(data => {
      return Promise.all([data, userJs.User.getById(data.idOfowner)])
    })
    .then(([data, user]) => {
      data['avaUrl'] = user.avaUrl;
      return Promise.all([data, commentJs.Comment.getAll(data.id)])
    })
    .then(([data, arrayofComments]) => {
      data.nameOfmyPenis = arrayofComments;
      res.status(200).render('post', { data: data });
    })
    .catch(err => 
      {console.log(err);
        res.status(404).redirect('https://9gag.com/404')});
});
app.get('/api/users/:id', function (req, res) {
  userJs.User.getById(req.params.id)
    .then(user => {
      user = JSON.stringify(user);
      res.set({ "Content-type": "application/json" }).status(200).send(user)
    })
    .catch(err => res.status(404).send(err))
});
///ти так експортиш всю парашу, яку ти написав в app.js до www
module.exports = app;


