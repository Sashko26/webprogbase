let searchStr;
let homer = "5db5e759d00d1de2f76ada0a";
const postJs = require('./post.js');
//additional functions
function PaginationAndSearch(req, data) {
    if (searchStr === undefined && req.query.searchStr === undefined) {
      data['searchStr'] = "";
    }
    else if (req.query.searchStr === undefined && searchStr !== undefined) {
      data['searchStr'] = searchStr;
    }
    else if (req.query.searchStr !== undefined) {
      searchStr = req.query.searchStr;
      data['searchStr'] = req.query.searchStr;
    }
    if (searchStr) {
      data.searchStr = searchStr;
      let items = [];
      for (let post of data.items) {
        if (post.name.includes(searchStr)) {
          items.push(post);
        }
      }
      data.items = items;
    }
    const sizeOfpage = 3;
    let page = req.query.page || 1;
    let amountOfPages = data.items.length / sizeOfpage;
    if (amountOfPages == 0) {
      if (req.query.searchStr != undefined) {
        data['weHaveNotСoincidence'] = true;
      }
      else {
        data['weHaveNotСoincidence'] = false;
      }
      amountOfPages = 1;
    }
    if (amountOfPages - parseInt(amountOfPages) !== 0) {
      amountOfPages = parseInt(amountOfPages) + 1;
    }
    if (page > amountOfPages) {
      page = amountOfPages;
    }
    if (page < 1) {
      page = 1;
    }
    let prevIsUnable = false;
    let nextIsUnable = false;
    let start = sizeOfpage * (page - 1);
    let end = sizeOfpage * page;
    data.items = data.items.slice(start, end);
    if (page == 1) {
      prevIsUnable = true;
    }
    if (page == amountOfPages) {
      nextIsUnable = true;
    }
    req.query.page = page;
    let nextPage = parseInt(page) + 1;
    let prevPage = page - 1;
  
    data["nextPage"] = nextPage;
    data["prevPage"] = prevPage;
    data["page"] = parseInt(page);
    data["prevIsUnable"] = prevIsUnable;
    data["nextIsUnable"] = nextIsUnable;
    data["amountOfPages"] = amountOfPages;
    return data;
  }
  function getPostFromRequestFromCreatingNewPost(topic, name, pictureUrl, content) {
    let date = new Date();
  //тут відбувається хардкоджена прив'язка юзера до поста.
    let post = new postJs.Post(homer, topic, name, date, pictureUrl, content);
    return post;
  }
  module.exports = {getPostFromRequestFromCreatingNewPost,PaginationAndSearch}; 