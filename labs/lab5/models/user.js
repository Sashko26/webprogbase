let fs = require('fs').promises
let mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    role: { type: String, default: "user" },
    login: { type: String, required: true },
    password: { type: String, required: false },
    fullname: { type: String, default: " " },
    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String },
    isDisabled: { type: Boolean },
    posts : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
    biography : { type: String}
});
const UserModel = mongoose.model('User', UserSchema);
class User {
    static getAll() {
        return UserModel.find();
    }
    static getById(id) {
        return UserModel.findById(id);
    }

}

module.exports = { User };



