
let fs = require('fs').promises;
let mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    content: { type: String },
    idOfowner: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    topic: { type: String },
    name: { type: String },
    dateOfpublication: { type: Date, default: Date.now },
    pictureUrl: { type: String },
    comments : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]

});
const PostModel = mongoose.model("Post", PostSchema);
class Post {

    constructor(idOfowner = 0, topic = null, name = null, dateOfpublication = null, pictureUrl = null, content = null) {
        this.content = content;
        this.idOfowner = idOfowner;
        this.topic = topic;
        this.name = name;
        this.dateOfpublication = dateOfpublication;
        this.pictureUrl = pictureUrl;
        
    }

    static insert(x) {
        return new PostModel(x).save();
    }
    static getAll() {
        return PostModel.find();
    }
    static getById(id) 
    {
        return PostModel.findById(id);     
    }
    static update(x) {
       
      return PostModel.findByIdAndUpdate(x.id, {comments : x.comments});
    }
    static deleteById(id) {
        return PostModel.findByIdAndDelete(id);
    }
}
module.exports = { Post };