const userFile = require('./models/user');
const postFile = require('./models/post');

let isWork = true;
let firstEntering = true;
console.clear();
const stdin = process.openStdin();
stdin.addListener("data", onInput);
console.log("Input something:");

function onInput(dataObject) {

    while (isWork === true) {
        let inputString;
        if (firstEntering === true) {
            console.clear();
            inputString = dataObject.toString().trim();
            firstEntering = false;
        }
        else {
            console.clear();
            console.log("Input something:");
            const readline = require('readline-sync');
            inputString = readline.question();
            console.clear();
        }
        console.log("Your input: " + inputString);
        let regUserGet = /users\/get\/[0-9]/;
        let regPostGet = /posts\/get\/[0-9]/;
        let regPostDelete = /posts\/delete\/[0-9]/;
        let regPostInsert = /posts\/insert/;
        let regPostUpdate = /posts\/update\/[0-9]/;

        if (inputString.search(regUserGet) !== -1) {
            let num = inputString.match(/\d+/g);
            let realNum = Number(num);
            let value = userFile.User.getById(realNum);
            for (let i in value) {
                console.log(i + ": " + value[i]);
            }
        }
        else if (inputString.search(regPostInsert) !== -1) {
            const readline = require('readline-sync');
            const idOfowner = parseInt(readline.question("What is 'id' of owner?\n"));
            const topic = readline.question("What is topic of your post?\n");
            const name = readline.question("What is name of your post?\n");
            const dateOfpublication = new Date();
            let newPost = new postFile.Post(0, idOfowner, topic, name, dateOfpublication);
            postFile.Post.insert(newPost);
        }
        else if (inputString.search(regPostUpdate) !== -1) {
            const readline = require('readline-sync');
            let num = inputString.match(/\d+/g);
            let id = Number(num);
            let boolean = true;
            let post =  postFile.Post.getById(id);
            while (boolean) {
                console.log("ID of owner: "+post.idOfowner+","+" topic: "+post.topic+","+ " name: "+'"'+post.name+'"'+".");
                console.log("1.Change owner");
                console.log("2.Change topic");
                console.log("3.Change name");
                console.log("4.Сomplate updating");
                let buf = readline.question();
                console.clear();
                switch (buf) {
                    case '1':
                        {
                            let idOfownerStr;
                            let isNum = false;
                            while (isNum === false) {
                                idOfownerStr = readline.question("What is 'id' of new owner?\n");
                               
                                if (typeof parseInt(idOfownerStr) === "number" && parseInt(idOfownerStr)>0) { isNum = true; }
                                console.clear();
                            }
                            post.idOfowner = parseInt(idOfownerStr);
                            
                        } break;
                    case '2':
                        {
                            let topic = readline.question("What is new topic of post?\n");
                            console.clear();
                            while (topic.length === 0) {
                                topic = readline.question("What is new topic of post?\n");
                                console.clear();
                            }
                            post.topic = topic;
                            

                        } break;
                    case '3':
                        {
                            let name = readline.question("What is new name of post?\n");
                            console.clear();
                            while (name.length === 0) {
                                name = readline.question("What is new name of post?\n");
                                console.clear();
                            }
                            post.name = name;
                        } break;
                    case '4':
                        {
                            boolean = false;
                            postFile.Post.update(post);
                        } break;
                    default:
                        {
                            console.log("hello");
                        }
                }
            }
        }
        else if (inputString.search(regPostGet) !== -1) {
            let num = inputString.match(/\d+/g);
            let realNum = Number(num);
            let value = postFile.Post.getById(realNum);
            for (let i in value) {
                console.log(i + ": " + value[i]);
            }

        }
        else if (inputString.search(regPostDelete) !== -1) {
            let num = inputString.match(/\d+/g);
            let realNum = Number(num);
            console.log(realNum);
            console.log(postFile.Post.deleteById(realNum,(err,data)=>
            {
                console.log(data);
                console.log(err);
                console.log("huyyyyyyy");

            }));
        }
        else if (inputString === "users") {
            let arr = userFile.User.getAll();
            let numInLeast = 1;
            for (let value of arr) {
                console.log(numInLeast + ") " + "id: " + value.id + "," + " login: " + value.login + "," + " fullname: " + value.fullname + ".");
                numInLeast++;
            };
        }
        else if (inputString === "posts") {
            let arr = postFile.Post.getAll();
            let numInLeast = 1;
            for (let value of arr) {
                console.log(numInLeast + ") " + "id of owner: " + value.idOfowner + "," + " name: " + '"' + value.name + '"' + ".");
                numInLeast++;
            };
        }
        else {
            console.clear();
        }

        let inCorrectWork = true;
        while (inCorrectWork) {
            console.log("Do you want to continue use the program?");
            console.log("1.Yes");
            console.log("0.No");
            const readline = require('readline-sync');
            let fate = readline.question();
            switch (fate) {
                case '1':
                    {
                        inCorrectWork = false;
                        isWork = true;
                    } break;
                case '0':
                    {
                        inCorrectWork = false;
                        isWork = false;
                        console.clear();
                    } break;
            }

        }

    }

}








/* } */


