class Post {
    constructor(id = 0, idOfowner = 0, topic = null, name = null, dateOfpublication = null) {
        this.id = id;
        this.idOfowner = idOfowner;
        this.topic = topic;
        this.name = name;
        this.dateOfpublication = dateOfpublication;
    }



    static insert(x) {
        let fs = require('fs');
        let textJson = fs.readFileSync("./data/posts.json", 'utf8');
        let text = JSON.parse(textJson);
        x.id = text.nextId;
        text.nextId++;
        text.items.push(x);
        textJson = JSON.stringify(text, null, 4);
        fs.writeFileSync("./data/posts.json", textJson);
        return x.id;
    }
    static getAll() {
        let fs = require('fs');
        let textJson = fs.readFileSync("./data/posts.json", 'utf8');
        let text = JSON.parse(textJson);
        return text.items;
    }
    static getById(id) {

        let fs = require('fs');
        let textJson = fs.readFileSync("./data/posts.json", 'utf8');
        let text = JSON.parse(textJson);
        for (let i of text.items) {
            if (i.id === id) {
                return i;
            }
        }

    }
    static update(x) {
        let fs = require('fs');
        let textJson = fs.readFileSync("./data/posts.json", 'utf8');
        let text = JSON.parse(textJson);
        for (let i of text.items) {
            if (i.id === x.id) {
                let idOwnerChanged = true;
                let topicChanged = true;
                let nameChanged = true;
                if (x.idOfowner === null) {
                    x.idOfowner === i.idOfowner;
                    idOwnerChanged = false;
                }
                if (x.topic === null) {
                    x.topic === i.topic;
                    topicChanged = false;
                }
                if (x.name === null) {
                    x.name === i.name;
                    nameChanged = false;
                }
                if (x.dateOfpublication === null) {
                    x.dateOfpublication === i.dateOfpublication;
                }
                if (idOwnerChanged === true) {
                    i.idOfowner = x.idOfowner;
                }
                if (topicChanged === true) {
                    i.topic = x.topic;
                }
                if (nameChanged === true) {
                    i.name = x.name;
                }
                textJson = JSON.stringify(text, null, 4);
                fs.writeFileSync("./data/posts.json", textJson);
                return true;
            }
        }
        return false;
    }
    /*   static deleteById(id) {
          let fs = require('fs');
          let textJson = fs.readFileSync("./data/posts.json", 'utf8');
          let text = JSON.parse(textJson);
          let arr = text.items;
          for (let i = 0; i < arr.length; i++) {
              if (arr[i].id === id) {
                  for (let j = i; j < arr.length - 1; j++) {
                      text.items[j] = text.items[j + 1];
                  }
                  console.log(text.items.pop());
                  break;
              }
          }
          textJson = JSON.stringify(text, null, 4);
          fs.writeFileSync("./data/posts.json", textJson);
  
      } */
    static deleteById(id, callback) {
        let fs = require('fs');
        fs.readFile("./data/posts.json", 'utf8', (err, data) => {
             data = JSON.parse(data);
            let arr = data.items;
            let deletedPost;

            for (let i = 0; i < arr.length; i++) {
                if (arr[i].id === id) {
                    deletedPost = arr[i];
                    for (let j = i; j < arr.length - 1; j++) {
                        data.items[j] = data.items[j + 1];
                    }
                    console.log(data.items.pop());
                    break;
                }
            }
            data = JSON.stringify(data, null, 4);
            fs.writeFile("./data/posts.json", data, () => {
                if (deletedPost == undefined) {
                    let err = new Error("This with ID " + id + " do not exists!");
                    callback(err, null);
                }
                callback(null, deletedPost);
            });

        });


    }
}

module.exports = { Post };